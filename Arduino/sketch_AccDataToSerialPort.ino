#include <SoftwareSerial.h>


int sensorPinPoti = A0;
int sensorPinBicX = A1;
int sensorPinBicY = A2;
int sensorPinTriX = A3;
int sensorPinTriY = A4;
int sensorPinEMG = A5;

int sensorNames[] = {sensorPinPoti,sensorPinBicX,sensorPinBicY,sensorPinTriX, sensorPinTriY,sensorPinEMG};

float sensorVals[] = {0,0,0,0,0,0};

double time;

void setup()
{ 
  Serial.begin(115200); // or 3456000 or 500000

  Serial.print("poti");
  Serial.print(","); // separator of 1 char iso \n\r which is two char
  Serial.print("BX");
  Serial.print(","); // separator of 1 char iso \n\r which is two char
  Serial.print("BY");
  Serial.print(","); // separator of 1 char iso \n\r which is two char
  Serial.print("TX");
  Serial.print(","); // separator of 1 char iso \n\r which is two char
  Serial.print("TY");
  Serial.print(","); // separator of 1 char iso \n\r which is two char
  Serial.print("EMG");
  Serial.print(","); // separator of 1 char iso \n\r which is two char
  Serial.print("time");
  Serial.print("\n"); // separator of 1 char iso \n\r which is two char

  //mySerial = new Serial( this, Serial.list()[0], 9600 );
  //output = createWriter( "data.txt" );
   
  time = millis();
}

void loop()
{ 
  if(millis() - time < 10000)
  {
    sensorVals[0] = analogRead(sensorPinPoti);
    sensorVals[1] = analogRead(sensorPinBicX);
    sensorVals[2] = analogRead(sensorPinBicY);
    sensorVals[3] = analogRead(sensorPinTriX);
    sensorVals[4] = analogRead(sensorPinTriY);
    sensorVals[5] = analogRead(sensorPinEMG);

    Serial.print(sensorVals[0]);
    Serial.print(",");
    Serial.print(sensorVals[1]);
    Serial.print(",");
    Serial.print(sensorVals[2]);
    Serial.print(",");
    Serial.print(sensorVals[3]);
    Serial.print(",");
    Serial.print(sensorVals[4]);
    Serial.print(",");
    Serial.print(sensorVals[5]);
    Serial.print(",");
    Serial.print(millis() - time);
    Serial.print("\n");
    
    //for(int i = 0; i < sizeof(sensorVals); i++)
    //{
     // sensorVals[i] = analogRead(sensorNames[i]);
      //Serial.print(sensorVals[i]);
      //Serial.print(","); 
    //}
    //Serial.print(millis() - time);
    //Serial.print("\n");
  }
}



