## Beschreibung

Der vorliegende Code ermöglicht das Aufnehmen und Klassifizieren von Datensätzen, wie in der zugehörigen Bachelorarbeit beschrieben. 

## Aufnehmen neuer Datensätze

Die Sensoren müssen wie in Abschnitt 3.2 beschrieben am Arduino angeschlossen sein. Der Arduino wird mittels USB-Kabel am PC angeschlossen. Die mitgelieferte .ino-Datei wird auf dem Arduino installiert und anschließend können für 10 Sekunden am *Seriellen Monitor* die Daten in einer csv-Formatierung ausgelesen werden. 
Die Daten werden in eine .txt-Datei geschrieben und unter dem Pfad `/Data/PFD/<Gewicht>/forTesting` oder `/Data/PFD/<Gewicht>/forTraining` abgelegt.

## Attribute extrahieren

Die rohen Sensordaten werden jetzt so bearbeitet, dass eine Datentabelle wie in Abschnitt 5.2 entsteht. Das Extrahieren dieser Datentabellen kann gesammelt für alle Sensordaten in `/Data/PFD/<Gewicht>/forTesting` oder `/Data/PFD/<Gewicht>/forTraining` durchgeführt werden. Hierfür wird die Funktion `MakeDataReadyForR()` in Matlab aufgerufen. Für alle Trainingsdaten wird gebündelt eine Datentabelle in `/Data/<NameTrainingstabelle>` erstellt. Die Testdaten werden direkt in ihrem jeweiligen Verzeichnis für jede Eingangsdatentabelle erstellt.   

## Klassifikation erstellen

Für die Klassifikation wird das Skript *classify.R* verwendet. Diese kann durchgeführt werden durch ausführen der Funktion `doPredictionForWeightClass`. Hierbei muss der jeweilige Gewichtsabschnitt als String mitgegeben werden und eine 1 oder 0, je nachdem ob ein neuer Random Forest trainiert werden soll oder ein bestimmter bestehender genutzt werden soll. 


## Code Beispiel

Das folgende Codebeispiel erstellt aus bestehenden Datensätzen die für die Klassifizierung in R benötigte Datentabelle. Die bestehenden Datensätze müssen das durch das Arduino Skript vorgegebene Format haben und sich in dem Pfad `/Data/PFD/5KG/forTesting` und `/Data/PFD/5KG/forTraining` befinden. 
```Matlab
MakeDataReadyForR('5KG', 1);
```
Es wird hierdurch eine neue Datei unter `/Data/trainData5KG.csv` erstellt, in der die Trainingsdaten mit den extrahierten Features zusammengefasst sind. 
Für jede weitere Sensordatendatei im Pfad `/Data/PFD/5KG/forTesting` wird eine neue Datei erstellt, die für das Testen des Klassifikators verwendet werden kann.

---

Das nächste Codebeispiel erstellt aus den Trainingsdaten einen Klassifikator und klassifiziert anhand dessen die Testdaten. Die Trainings- und Testdaten müssen sich hierfür in den entsprechenden Ordnern befinden und die verschiedenen Features müssen bereits extrahiert worden sein. 
```R
doPredictionForWeightClass('5KG', 1);
```
---

Um sich die Ergebnisse der Klassifizierung in einem Plot mit dem Ground-Truth-Wert auszugeben, kann in Matlab folgender Befehl verwendet werden:
```Matlab
ClassificationOverview('5KG','KlassifikationsName','BenutzteFeatures');
```
Die Features *KlassifikationsName* und *BenutzteFeatures* dienen hierbei lediglich zur Erzeugung der Überschrift. Der Parameter *5KG* gibt den Gewichtsstring an, von welchem der Pfad in dem nach der Datei gesucht wird abhängig ist. 
