source("evaluation/evaluatePredictions.r")
library(randomForest)
library(rpart)
#library(randomForest.ddR)
library(ranger)

classifyRF <- function(trainData, classData, formula, newRF){
    # model bauen
    print("Building rf")
    message(newRF)
    createNewRF <- newRF;
    indexes <- which(trainData$pcd == 0, arr.ind = TRUE);
    indexes <- sample(indexes, length(indexes)-3*sum(trainData$pcd));
    trainData <- trainData[-indexes,];
    indexes = indexes[-(1:(length(indexes)-sum(trainData$pcd)))];
    if(createNewRF)
    {
        rf <- randomForest(formula, data = trainData, ntree=1000);
        save(rf,file = "RandomForest_WithoutAcc.RData")
    }
    else
    {
      cwd <- getwd();
      #use own path to RF
      setwd('/Users/leonard.adolphs/Dropbox/Uni/DB_6.Semester/Bachelorarbeit/DataForRF');
      rf <- get(load("RandomForest_WithoutAcc.RData"));
      setwd(cwd);
    }
    # model auswerten
    print("Predicting")
    start.time <- Sys.time()
    prob = predict(rf,newdata=classData,type="response")
    end.time <- Sys.time()
    time.taken <- end.time - start.time
    message(paste('== Time taken: ', time.taken));
    # Auswertung an Datenframe anhaengen
    classData$prediction <- abs(prob);
    returnList = list("classData" = classData, "rf" = rf);
    returnList
}


# Ranger (Fast random forest)
classifyRanger <- function(trainData, classData, formula, numTrees = 100){
  # model bauen
  print("Building rf")
  rf = ranger(formula=formula, data = trainData, num.trees=numTrees, write.forest = TRUE, num.threads = 4, classification = TRUE)
  
  importance <- importance(rf);
  # model auswerten
  print("Predicting")
  prob = predict(rf,dat=classData)
  
  # Auswertung an Datenframe anhaengen
  # Da die Klassen ausgegeben werden und die bei as.numeric auf 1 und 2 abgebildet werden wird hier -1 gerechnet
  classData$prediction <- prob$predictions
  returnList = list("classData" = classData, "importance" = importance);
  returnList
}


