function ClassificationOverView(weightStr, classification, features)  
listing = dir(strcat('../Data/PFD/',weightStr,'/forTesting'));
directionIndexesToDelete = [];

for i = 1:length(listing)
    if length(strfind(listing(i).name, 'Weight')) < 1
       directionIndexesToDelete = [directionIndexesToDelete i];
    elseif length(strfind(listing(i).name, 'classD')) < 1
        directionIndexesToDelete = [directionIndexesToDelete i];
    elseif length(strfind(listing(i).name, 'Pred')) < 1
        directionIndexesToDelete = [directionIndexesToDelete i];
        elseif length(strfind(listing(i).name, 'EMG')) > 0
        directionIndexesToDelete = [directionIndexesToDelete i];
        %-----------for statistical testing
   % elseif length(strfind(listing(i).name, 'stat')) < 1
   %     directionIndexesToDelete = [directionIndexesToDelete i];
    elseif length(strfind(listing(i).name, '5KGWeight')) < 1
        directionIndexesToDelete = [directionIndexesToDelete i];
    end
end    

listing(directionIndexesToDelete) = [];

indexesToDelete = [1,2,3,16,17,18,19,20,21,32,33,34];
listing(indexesToDelete) = [];
%-----------for statistical testing: indexes
%listing = listing([1 2 3 4 6 7 8 9 10]);

count = length(listing);
cols = 6;
rows = ceil(count / cols);

figure(1)
titleStr = strcat('Klassifikator: ' ,classification, ' | Features: ',features, ' | Gewicht: ', weightStr);
%suptitle(titleStr)

for i = 1:count
    subplot(rows,cols,i)
    originalD = strrep(listing(i).name, 'classD_', '');
    originalD = strrep(originalD, '_Pred', '');
    originalD = strrep(originalD, '.csv', '.txt');

    ShowClassificationResults(originalD, listing(i).name, i, weightStr);
end
end