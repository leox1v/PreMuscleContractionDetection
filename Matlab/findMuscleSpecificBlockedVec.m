function blocked = findMuscleSpecificBlockedVec(poti, timeVec)
    potHighs = find(poti == 1);
    timeseries = findTimeSeries(potHighs, length(timeVec), 10);
    blocked = timeseries;
end