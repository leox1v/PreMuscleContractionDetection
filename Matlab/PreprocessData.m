%PreprocessData
function PreprocessData(weightStr, forcePreprocess)

if forcePreprocess
    %preprocess trainTable
listing = dir(strcat('../DataForRF/PFD/',weightStr,'/forTraining'));
directionIndexesToDelete = [];
for i = 1:length(listing)
    if length(strfind(listing(i).name, 'Weight')) < 1
       directionIndexesToDelete = [directionIndexesToDelete i];
    elseif length(strfind(listing(i).name, 'classD')) > 0
        directionIndexesToDelete = [directionIndexesToDelete i];
    end
    if length(strfind(listing(i).name, 'Desi')) < 1
        directionIndexesToDelete = [directionIndexesToDelete i];
    end
end

listing(directionIndexesToDelete) = [];
trainTable = [];

for i = 1:length(listing)
    trainTable = [trainTable;PrepareDataForClassification(listing(i).name,1,weightStr)];
end

filename = strcat('../Data/trainData',weightStr,'.csv');
writetable(trainTable, filename);
end

%preprocess testTable - Pfad anpassen hier 
listing = dir(strcat('../Data/PFD/',weightStr,'/forTesting'));
directionIndexesToDelete = [];
alreadyClassd = {};
for i = 1:length(listing)
    if length(strfind(listing(i).name, 'Weight')) < 1
       directionIndexesToDelete = [directionIndexesToDelete i];
       elseif length(strfind(listing(i).name, 'classD')) > 0
        directionIndexesToDelete = [directionIndexesToDelete i];
        if length(strfind(listing(i).name, '_Pred')) < 1
            %alreadyClassd = [alreadyClassd listing(i).name];
        end
    end
end

listing(directionIndexesToDelete) = [];

%for i = 1:length(alreadyClassd)
%    alreadyClassd{i} = strrep(alreadyClassd{i},'classD_','');
%    alreadyClassd{i} = strrep(alreadyClassd{i},'.csv','.txt');
%end

testTable = [];

for i = 1:length(listing)
    testTable = [testTable;PrepareDataForClassification(listing(i).name,0,weightStr)];
end

filename = strcat('../Data/classData',weightStr,'.csv');
writetable(testTable, filename);
end