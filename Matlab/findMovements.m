function m = findMovements(potiValue, timeframe, tAxis)

deltaTime = round(0.1* length(tAxis) / timeframe);

sensibility = 0.04;
recognizedMovementsUp = [];
recognizedMovementsDown = [];
seriesCountUp = [];
seriesCountDown= [];

for i = 1+deltaTime:deltaTime:length(tAxis)
    if potiValue(i) > potiValue(i - deltaTime) + sensibility
        recognizedMovementsUp = [recognizedMovementsUp i];
    elseif potiValue(i) < potiValue(i - deltaTime) - sensibility
        recognizedMovementsDown = [recognizedMovementsDown i];
            
    end
end

%If Movement has been recognized you need to add the startingPoint by
%subtracting deltaTime
if length(recognizedMovementsUp) > 0
    if recognizedMovementsUp(1) - deltaTime > 0
        recognizedMovementsUp = [recognizedMovementsUp(1) - deltaTime recognizedMovementsUp];
    else
        recognizedMovementsUp = [1 recognizedMovementsUp];
    end
    
    seriesCountUp = 1;
    indexToInsert = [];
    valueToInsert = [];
    if length(recognizedMovementsUp) > 1
    for i = 2:length(recognizedMovementsUp)
        if recognizedMovementsUp(i) - deltaTime ~= recognizedMovementsUp(i-1)
            indexToInsert = [indexToInsert i];
            valueToInsert = [valueToInsert recognizedMovementsUp(i) - deltaTime];
            if recognizedMovementsUp(i) - 2* deltaTime == recognizedMovementsUp(i-1)
                seriesCountUp(end) = seriesCountUp(end) + 2;
            else
                seriesCountUp = [seriesCountUp 2];
            end
        else
            seriesCountUp(end) = seriesCountUp(end) + 1;
        end
    end
    else
        indexToInsert = 1
         if recognizedMovementsUp(1) - deltaTime > 0
         valueToInsert = recognizedMovementsUp(1) - deltaTime;
         else
            valueToInsert = 1;
         end
    end   
    if length(indexToInsert) > 0
        for i = 1:length(indexToInsert)
            recognizedMovementsUp = [recognizedMovementsUp(1:indexToInsert(i)-2+i) valueToInsert(i) recognizedMovementsUp(indexToInsert(i)-1+i:length(recognizedMovementsUp))]
        end  
    end
end

if length(recognizedMovementsDown) > 0
    if recognizedMovementsDown(1) - deltaTime > 0
        recognizedMovementsDown = [recognizedMovementsDown(1) - deltaTime recognizedMovementsDown];
    else
        recognizedMovementsDown = [1 recognizedMovementsDown];
    end
    
    seriesCountDown = 1;
    indexToInsert = [];
    valueToInsert = [];
    if length(recognizedMovementsDown) > 1
        for i = 2:length(recognizedMovementsDown)
        if recognizedMovementsDown(i) - deltaTime ~= recognizedMovementsDown(i-1)
            indexToInsert = [indexToInsert i];
            valueToInsert = [valueToInsert recognizedMovementsDown(i) - deltaTime];
            if recognizedMovementsDown(i) - 2*deltaTime == recognizedMovementsDown(i-1)
                seriesCountDown(end) = seriesCountDown(end) + 2;
            else
               seriesCountDown = [seriesCountDown 2]; 
            end
        else
            seriesCountDown(end) = seriesCountDown(end) + 1;
        end
        end
    else
        indexToInsert = 1
        if recognizedMovementsDown(1) - deltaTime > 0
            valueToInsert = recognizedMovementsDown(1) - deltaTime;
        else
            valueToInsert = 1;
        end
        
    end
    if length(indexToInsert) > 0
        for i = 1:length(indexToInsert)
            recognizedMovementsDown = [recognizedMovementsDown(1:indexToInsert(i)-2+i) valueToInsert(i) recognizedMovementsDown(indexToInsert(i)-1+i:length(recognizedMovementsDown))]
        end  
    end
end


m = struct('recMoveUp', recognizedMovementsUp, 'recMoveDown', recognizedMovementsDown, 'countUp', seriesCountUp , 'countDown' , seriesCountDown);

end