function accuracy = MatlabArduinoReadFromTxt(filePath, showFigure, hasTriceps, sensibility)
minF = 23;
maxF = 27;

if ~exist('sensibility','var')
     % third parameter does not exist, so default it to something
      sensibility = 0.0035;
end

linearPotiValue = 4;

if length(strfind(filePath, '60sek')) > 0
     timeframe = 60;
     linearPotiValue = 5;
else
    timeframe = 10;
end

if length(strfind(filePath, 'Desi')) > 0
       linearPotiValue = 4;
end
 
%T = readtable('../PreFilteredData/PFD_5KGWeight_NoDist_60sek.txt')
T = readtable(strcat('../PreFilteredData/',filePath));

x = getVoltageValue(T.BX);
y = getVoltageValue(T.BY);
z = getVoltageValue(T.TX);
emg = getVoltageValue(T.EMG);
tAxis = T.time / 1000;
poti = getVoltageValue(T.poti);

allAccX = getGValue(x);
allAccY = getGValue(y);
allAccZ = getGValue(z);

timeVecX = linspace(0,timeframe, length(allAccX));
timeVecY = linspace(0,timeframe, length(allAccY));
timeVecZ = linspace(0,timeframe, length(allAccZ));

newPoti = [];
for c = 1:length(poti)
    currentPotiValue = poti(c);
    normalizedValue = -(currentPotiValue - linearPotiValue) /1.8;
      if normalizedValue > 1
          normalizedValue = 1;
      end
    newPoti = [newPoti, normalizedValue];
end

poti = newPoti;
newEMG = [];
for c = 1:length(emg)
    currentEMGValue = emg(c);
    normalizedValue = (currentEMGValue - 1.55) /0.3;
      if normalizedValue > 1
          normalizedValue = 1;
      elseif normalizedValue < 0
              normalizedValue = 0;
      end
    newEMG = [newEMG, normalizedValue];
end 

emg = newEMG;



if showFigure
    figure(1)
    %subplot(4,2,1)
    %subplot(2,2,1)
    plot(tAxis,allAccX,'color','r'); hold on;
    %plot(tAxis,allAccY,'color','b', 'LineWidth',1); hold on;
    %plot(tAxis,allAccZ, 'color', 'y', 'LineWidth',1)
    ylabel('Accelerometer Werte [9,81 m / s^2]');
    xlabel('Zeit [s]');
    legend('X-Werte')%, 'Y-Werte', 'Z-Werte');
    axis([0, timeframe, -3 , 3]);
    grid
    drawnow;
    
    subplot(4,2,2)
    %subplot(2,2,2)
    plot(tAxis, poti);
    hold on;
    %plot(tAxis, emg);
    xlabel('Zeit [s]');
    ylabel('Anspannung des Muskels in %');
    axis([0, timeframe, 0,1]);
    grid;
    drawnow;
    
    hold off;
    
    %Find and plot movements of the arm using a smart derivation from the
%PotiValues
movements = findMovements(poti, timeframe, tAxis);
maxI = 0;
blockedVec{1} = [];

for i = 1:length(movements.countUp)
    currentStartIndex = 1;
    if i > 1
        currentStartIndex = sum(movements.countUp(1:i-1)) + 1;
    end
    currentEndIndex = sum(movements.countUp(1:i));
    
    currentStart = tAxis(movements.recMoveUp(currentStartIndex));
    currentEnd = tAxis(movements.recMoveUp(currentEndIndex));
    
    blockedVec{i} = [movements.recMoveUp(currentStartIndex) movements.recMoveUp(currentEndIndex)];
    maxI = i;
    
    patch([currentStart currentEnd  currentEnd currentStart],[0 0 1 1],'k',...
    'facecolor','g','edgecolor','g','FaceAlpha',0.25) ;
   
end

for i = 1:length(movements.countDown)
    currentStartIndex = 1;
    if i > 1
        currentStartIndex = sum(movements.countDown(1:i-1)) + 1;
    end
    currentEndIndex = sum(movements.countDown(1:i));
    
    currentStart = tAxis(movements.recMoveDown(currentStartIndex));
    currentEnd = tAxis(movements.recMoveDown(currentEndIndex));
    
    blockedVec{maxI + i} = [movements.recMoveDown(currentStartIndex) movements.recMoveDown(currentEndIndex)];
    
    patch([currentStart currentEnd  currentEnd currentStart],[0 0 1 1],'k',...
    'facecolor','b','edgecolor','b','FaceAlpha',0.25) ;
    %rectangle('Position',[currentStart abs(poti(movements.recMoveDown(currentEndIndex)) - poti(movements.recMoveDown(currentStartIndex))) currentEnd - currentStart poti(movements.recMoveDown(currentStartIndex))],'LineWidth',1, 'edgecolor', 'r');
end

%find blocked Vec because Poti Values too high
% blocked = findMuscleSpecificBlockedVec(poti, timeVecX);
% for i = 1:length(blocked.minValues)
%     blockedVec{length(blockedVec)+1} = [blocked.minValues(i) blocked.minValues(i)+blocked.distValues(i)];
% end



end 


% Section for plots for Ausarbeitung

%window allAccY
%N = 10;
%n = 0:N-1;

%figure(2)

%plot(timeVecX,allAccY)
%hold on;
%xlabel('Zeit [s]');
%ylabel('Beschleunigung [9.81 * m/s^2]');
%axis([0, timeframe, min(allAccY), max(allAccY)]);
%rectangle('Position',[5 min(allAccY) 1 max(allAccY)-min(allAccY)],'LineWidth',1.5);
%subplot(2,1,2);




   %% Wavelet Plot
   
dt = 1/length(tAxis);

NumVoices = 16;
a0 = 2^(1/NumVoices);
wavCenterFreq =  centfrq('morl');        %morl
%wavCenterFreq = 4*pi/(6+sqrt(2+6^2))    %morl2
%wavCenterFreq = 5/(2*pi);               %bump
maxfreq = 35;
minfreq = 5;
minscale = wavCenterFreq/(maxfreq*dt);
maxscale = wavCenterFreq/(minfreq*dt);
minscale = floor(NumVoices*log2(minscale));
maxscale = ceil(NumVoices*log2(maxscale));
scales = a0.^(minscale:maxscale).*dt;  

%Do CWT
cwtX = cwtft({allAccX,dt},'Scales',scales,'Wavelet','morl');

%Filter Unused frequencies over all times
freqidx = (cwtX.frequencies <7 | cwtX.frequencies > 30);
timeidx = 1:length(timeVecX);
cwtX.cfs(freqidx,timeidx) = 0;              %set values to nil
%xrec = icwtft(reconcwt);                   %reconstructed time signal


%Find maximum in power values
absoluteValues = abs(cwtX.cfs);
powerValues = absoluteValues((cwtX.frequencies > minF & cwtX.frequencies < maxF), 1:end); %davor von 15-30
trashedLowFrequencies = absoluteValues((cwtX.frequencies <=7), 1:end);
[numberOftrashedFrequ, B] = size(trashedLowFrequencies);
[maxValue, linearIndexesOfMaxes] = max(powerValues(:));
[rowsOfMaxes colsOfMaxes] = find(powerValues == maxValue);
[rowsOfPotHighs colsOfPotHighs] = find(powerValues >= sensibility);%0.9*maxValue);

%find timeValues
sizeTAxis = length(tAxis);
timeseries = findTimeSeries(colsOfPotHighs, sizeTAxis, timeframe);
if length(blockedVec{1}) > 0
    timeseries = cleanTimeSeries(timeseries, blockedVec);
end

%convert timeseries to relative timeseries
minRelT = timeframe* timeseries.minValues / sizeTAxis;
distRelT = timeframe* timeseries.distValues / sizeTAxis;

%plot CWT x-Axis in 'contour' with marked muscle flex
if showFigure
figure(1);
subplot(4,2,[3:4]);
contour(timeVecX,cwtX.frequencies(cwtX.frequencies < 35),abs(cwtX.cfs(cwtX.frequencies < 35, 1:end)));
xlabel('Seconds'), ylabel('Hz');
grid on;
title('Analytic CWT using Morlet Wavelet with filter')
hcol = colorbar;
hcol.Label.String = 'Magnitude';

%Rectangle around flexingTime
for i = 1:length(timeseries.minValues)
    rectangle('Position',[minRelT(i) minF distRelT(i) maxF-minF],'LineWidth',1.5);
end

end
   
cwtXTri = cwtft({allAccZ,dt},'wavelet','morl','scales',scales);

%Filter Unused frequencies from Triceps over all times
freqidx = (cwtXTri.frequencies <15 | cwtXTri.frequencies > 30);
timeidx = 1:length(timeVecZ);
cwtXTri.cfs(freqidx,timeidx) = 0;              %set values to nil

if showFigure
figure(1);
subplot(4,2,[5:6]);
contour(timeVecZ,cwtXTri.frequencies(cwtXTri.frequencies < 35),abs(cwtXTri.cfs(cwtXTri.frequencies < 35, 1:end)));
xlabel('Seconds'), ylabel('Hz');
grid on;
if(hasTriceps == 1)
    title('Analytic CWT from Triceps Signal using Morlet Wavelet with filter')
else
    title('Analytic CWT from Y Signal using Bump Wavelet with filter')
end
hcol = colorbar;
hcol.Label.String = 'Magnitude';
%helperCWTTimeFreqPlot(cwtXTri.cfs,timeVecZ,cwtXTri.frequencies,...
%    'surf','CWT of xValue -- Morl Wavelet','Seconds','Hz');   
end

%cwtY = cwtft({allAccY,dt},'wavelet','morl','scales',scales);
%figure(1);
%subplot(4,2,[7:8]);
%helperCWTTimeFreqPlot(cwtY.cfs,timeVecY,cwtY.frequencies,...
%    'surf','CWT of yValue -- Morl Wavelet','Seconds','Hz');

%Test how well the predictions are 
%Fehler 1.Art
minDeltaDist = 1 * sizeTAxis / timeframe;
deltaPotivalues = poti(timeseries.minValues + timeseries.distValues) - poti(timeseries.minValues);
for i = 1:length(deltaPotivalues)
    if deltaPotivalues(i) < 0.15 & abs(deltaPotivalues(i)) < 0.15 & timeseries.distValues(i) < 2* minDeltaDist
        currentPotiMinValue = poti(timeseries.minValues);
        currentPotiMinValue = currentPotiMinValue(i);
        if (timeseries.minValues(i) + timeseries.distValues(i) + minDeltaDist) > sizeTAxis
            newValue = max(poti(timeseries.minValues(i) + timeseries.distValues(i) : sizeTAxis)) - currentPotiMinValue;
            if length(newValue) > 0
                deltaPotivalues(i) = newValue;
            else
                deltaPotivalues(i) = 0;
            end
        else
            if timeseries.minValues(i) + timeseries.distValues(i) + round(2*minDeltaDist) <= length(poti)
                deltaPotivalues(i) = max(poti(timeseries.minValues(i) + timeseries.distValues(i) : timeseries.minValues(i) + timeseries.distValues(i) + round(2*minDeltaDist))) - currentPotiMinValue;
            else
                deltaPotivalues(i) = max(poti(timeseries.minValues(i) + timeseries.distValues(i) : length(poti))) - currentPotiMinValue;
            end
        end
    end
end

rightPredictions = deltaPotivalues > 0.15;
accuracy = sum(rightPredictions) / length(deltaPotivalues);
accuracy(isnan(accuracy)) = 1;
mistakeOrder1 = 1 - accuracy;

if showFigure
figure(1);
subplot(4,2,2);
colorArray(rightPredictions == 0) = 'r';
colorArray(rightPredictions == 1) = 'g';

%Fehler 2. Art
%flexes = findFlexes(poti, tAxis, timeframe);
%s = getCorrectlyIdentifiedFlexes(flexes.minX,flexes.maxX,minRelT,minRelT+distRelT);
%correctlyIdentified = s.correctlyIdentified;
%colorArray2(correctlyIdentified == 0) = 'k';
%colorArray2(correctlyIdentified == 1) = 'y';

%for i = 1:length(flexes.minX)
%    rectangle('Position',[flexes.minX(i) flexes.minY(i) flexes.maxX(i)-flexes.minX(i) flexes.maxY(i) - flexes.minY(i)],'LineWidth',1.5, 'edgecolor', colorArray2(i));
%end

%Fehler 1. Art
for i = 1:length(deltaPotivalues)
    if deltaPotivalues(i) < 0 
        yStart = poti(timeseries.minValues(i)) - abs(deltaPotivalues(i));
    else
        yStart = poti(timeseries.minValues(i));
    end
    rectangle('Position',[minRelT(i) yStart distRelT(i) abs(deltaPotivalues(i))],'LineWidth',1.5, 'edgecolor', colorArray(i));
end

legend('Normalisierter Potentiometerwert', 'Aufw�rtsbewegung', 'Abw�rtstbewegung','Kontraktion von Klassifikator richtig erkannt', 'Kontraktion von Klassifikator falsch erkannt');

%Text f�r Fehler 1. Art
mistake1Str = strcat(num2str(length(rightPredictions)-sum(rightPredictions)),'/',num2str(length(rightPredictions)))
accuracyInPerc = sprintf('%.0f%%',100*accuracy)

mytxt = uicontrol('Style','text','String','Fehler 1. Art',... 
                    'Position', [50 50 150 50],...
                    'FontSize' , 17, 'FontWeight', 'bold');
mytxt = uicontrol('Style','text','String','Fehler:',... 
                    'Position', [50 30 150 50],...
                    'FontSize' , 14, 'FontWeight', 'bold'); 
mytxt = uicontrol('Style','text','String','Genauigkeit:',... 
                    'Position', [15 10 185 50],...
                    'FontSize' , 14, 'FontWeight', 'bold');                
mytxt = uicontrol('Style','text','String',mistake1Str,... 
                    'Position', [150 30 150 50],...
                    'FontSize' , 14, 'FontWeight', 'bold');
mytxt = uicontrol('Style','text','String',accuracyInPerc,... 
                    'Position', [150 10 150 50],...
                    'FontSize' , 14, 'FontWeight', 'bold');
                
%Test f�r Fehler 2. Art
mytxt = uicontrol('Style','text','String','Fehler 2. Art',... 
                    'Position', [300 50 150 50],...
                    'FontSize' , 17, 'FontWeight', 'bold');
mytxt = uicontrol('Style','text','String','Fehler:',... 
                    'Position', [300 30 150 50],...
                    'FontSize' , 14, 'FontWeight', 'bold');
mytxt = uicontrol('Style','text','String','Genauigkeit:',... 
                    'Position', [265 10 185 50],...
                    'FontSize' , 14, 'FontWeight', 'bold');                
mytxt = uicontrol('Style','text','String','NA',... 
                    'Position', [400 30 150 50],...
                    'FontSize' , 14, 'FontWeight', 'bold');
mytxt = uicontrol('Style','text','String','NA',... 
                    'Position', [400 10 150 50],...
                    'FontSize' , 14, 'FontWeight', 'bold');                
%mytxt.Enable = 'Inactive';
%mytxt.ButtonDownFcn = 'disp(''Text was clicked'')';
%set(mytxt)

end



%for Ausarbeitung
figure(2)
contour(timeVecX,cwtX.frequencies(cwtX.frequencies < 35),abs(cwtX.cfs(cwtX.frequencies < 35, 1:end)));
xlabel('Zeit [s]')
ylabel('Frequenz [Hz]')
hold on
rectangle('Position',[3 18 .1 2],'LineWidth',2);
x = [0.5,0.37];
y = [0.5,0.5];
a = annotation('textarrow',x,y,'String','freq1820 = 0.0012')
a.FontSize = 25;
    
save(['/Users/leonard.adolphs/Dropbox/Uni/DB_6.Semester/Bachelorarbeit/MatlabCode/GatheredData/AccDatafile_',datestr(datetime('now')), '.mat'],'allAccX','allAccY','allAccZ','tAxis', 'poti')

end