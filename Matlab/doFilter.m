function y = doFilter(x)
%DOFILTER Filters input x and returns output y.

% MATLAB Code
% Generated by MATLAB(R) 9.0 and the DSP System Toolbox 9.2.
% Generated on: 28-Apr-2016 18:49:35

%#codegen

% To generate C/C++ code from this function use the codegen command.
% Type 'help codegen' for more information.

persistent Hd;

if isempty(Hd)
    
    % The following code was used to design the filter coefficients:
    %
    % Fstop1 = 10;   % First Stopband Frequency
    % Fpass1 = 15;   % First Passband Frequency
    % Fpass2 = 35;   % Second Passband Frequency
    % Fstop2 = 40;   % Second Stopband Frequency
    % Astop1 = 60;   % First Stopband Attenuation (dB)
    % Apass  = 1;    % Passband Ripple (dB)
    % Astop2 = 60;   % Second Stopband Attenuation (dB)
    % Fs     = 164;  % Sampling Frequency
    %
    % h = fdesign.bandpass('fst1,fp1,fp2,fst2,ast1,ap,ast2', Fstop1, Fpass1, ...
    %                      Fpass2, Fstop2, Astop1, Apass, Astop2, Fs);
    %
    % Hd = design(h, 'butter', ...
    %     'MatchExactly', 'stopband', ...
    %     'SystemObject', true);
    
    Hd = dsp.BiquadFilter( ...
        'Structure', 'Direct form II', ...
        'SOSMatrix', [1 0 -1 1 -0.408933054725654 0.933412087289548; 1 0 -1 ...
        1 -1.65744331035498 0.962941253352552; 1 0 -1 1 -1.59342262911643 ...
        0.892458762234165; 1 0 -1 1 -0.397847601481267 0.813622092571567; 1 0 -1 ...
        1 -1.52842663895148 0.825924025919099; 1 0 -1 1 -0.40219510066595 ...
        0.710320548254955; 1 0 -1 1 -1.46162356583503 0.762475545928848; 1 0 -1 ...
        1 -0.420155596176863 0.622136173584631; 1 0 -1 1 -1.3920847302506 ...
        0.701493927145539; 1 0 -1 1 -0.450610438206191 0.548108853029167; 1 0 -1 ...
        1 -1.31878896321024 0.642640028817726; 1 0 -1 1 -0.493014526597026 ...
        0.487774802752504; 1 0 -1 1 -1.24067494082078 0.585965249552635; 1 0 -1 ...
        1 -0.547244201519817 0.441228633397288; 1 0 -1 1 -1.15681585402041 ...
        0.532126309097986; 1 0 -1 1 -0.61334581570379 0.409139017570465; 1 0 -1 ...
        1 -1.0668371075905 0.482707986666979; 1 0 -1 1 -0.691061156931778 ...
        0.392609044907957; 1 0 -1 1 -0.971648845109836 0.440503345401581; 1 0 -1 ...
        1 -0.779054500745379 0.392668384408389; 1 0 -1 1 -0.874165865385051 ...
        0.409290257518013], ...
        'ScaleValues', [0.376664129517757; 0.376664129517757; ...
        0.359127172408571; 0.359127172408571; 0.344314779862674; ...
        0.344314779862674; 0.33190557906366; 0.33190557906366; ...
        0.321631036704702; 0.321631036704702; 0.313271778609927; ...
        0.313271778609927; 0.306652537699802; 0.306652537699802; ...
        0.301637153866192; 0.301637153866192; 0.298124251809558; ...
        0.298124251809558; 0.296043836481758; 0.296043836481758; ...
        0.295354871240993; 1]);
end

s = double(x);
y = step(Hd,s);

