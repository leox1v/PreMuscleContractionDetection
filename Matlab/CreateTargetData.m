%PreprocessData
function CreateTargetData(weightStr)

%preprocess testTable
listing = dir(strcat('../Data/PFD/',weightStr,'/forTesting'));
directionIndexesToDelete = [];
for i = 1:length(listing)
    if length(strfind(listing(i).name, 'Weight')) < 1
       directionIndexesToDelete = [directionIndexesToDelete i];
    elseif length(strfind(listing(i).name, '.txt')) < 1
        directionIndexesToDelete = [directionIndexesToDelete i];
    end
end

listing(directionIndexesToDelete) = [];
testTable = [];

for i = 1:length(listing)
    testTable = PrepareDataForClassification(listing(i).name,0,weightStr);
    name = strrep(listing(i).name,'.txt','')
    filename = strcat('../Data/PFD/',weightStr,'/forTesting/','classD_',name,'.csv');
    writetable(testTable, filename);
end

end