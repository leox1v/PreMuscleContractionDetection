function currentTable = PrepareDataForClassification(filePath, isTraining, weightStr)
minF = 23;
maxF = 27;
showFigure = 1;

linearPotiValue = 4;

if length(strfind(filePath, '60sek')) > 0
     timeframe = 60;
     linearPotiValue = 5;
else
    timeframe = 10;
end
 
%T = readtable('../PreFilteredData/PFD_5KGWeight_NoDist_60sek.txt')
if isTraining
    T = readtable(strcat('../DataForRF/PFD/',weightStr,'/forTraining/',filePath));
else
    T = readtable(strcat('../DataForRF/PFD/',weightStr,'/forTesting/',filePath));
end

bx = getVoltageValue(T.BX);
by = getVoltageValue(T.BY);
tx = getVoltageValue(T.TX);
ty = getVoltageValue(T.TY);
tAxis = T.time / 1000;
poti = getVoltageValue(T.poti);

allAccBX = getGValue(bx);
allAccBY = getGValue(by);
allAccTX = getGValue(tx);
allAccTY = getGValue(ty);

timeVecX = linspace(0,timeframe, length(allAccBX));

pcd = zeros(size(timeVecX));

newPoti = [];
for c = 1:length(poti)
    currentPotiValue = poti(c);
    normalizedValue = -(currentPotiValue - linearPotiValue) /1.8;
      if normalizedValue > 1
          normalizedValue = 1;
      end
    newPoti = [newPoti, normalizedValue];
end

poti = newPoti;

if showFigure
    figure(1)
    subplot(4,2,1)
    %subplot(2,2,1)
    plot(tAxis,allAccBX,'color','r'); hold on;
    plot(tAxis,allAccBY,'color','b'); hold on;
    plot(tAxis,allAccBX, 'color', 'y')
    ylabel('Accelerometer Werte [9,81 m / s^2]');
    xlabel('Zeit [s]');
    legend('X-Werte', 'Y-Werte', 'Z-Werte');
    axis([0, timeframe, -3 , 3]);
    grid
    drawnow;
    
    subplot(4,2,2)
    %subplot(2,2,2)
    plot(tAxis, poti);
    xlabel('Zeit [s]');
    ylabel('Anspannung des Muskels in %');
    axis([0, timeframe, 0,1]);
    grid;
    drawnow;
    
    tic

movements = findMovements(poti, timeframe, tAxis);
maxI = 0;
blockedVec{1} = [];

for i = 1:length(movements.countUp)
    currentStartIndex = 1;
    if i > 1
        currentStartIndex = sum(movements.countUp(1:i-1)) + 1;
    end
    currentEndIndex = sum(movements.countUp(1:i));
    
    currentStart = tAxis(movements.recMoveUp(currentStartIndex));
    currentEnd = tAxis(movements.recMoveUp(currentEndIndex));
    
    blockedVec{i} = [movements.recMoveUp(currentStartIndex) movements.recMoveUp(currentEndIndex)];
    maxI = i;
    
    patch([currentStart currentEnd  currentEnd currentStart],[0 0 1 1],'k',...
    'facecolor','g','edgecolor','g','FaceAlpha',0.25) ;

    deltaTime = round(0.5* length(tAxis) / timeframe);
    currentStart = round(currentStart* length(tAxis) / timeframe);
    currentEnd = round(currentEnd * length(tAxis) / timeframe);
    %pcd(round(currentStart-deltaTime):currentEnd) = 1;
    if round(currentStart-deltaTime) < 1
        currentStart = deltaTime + 1;
    end
    pcd(round(currentStart-deltaTime):round(currentStart+deltaTime*0.75)) = 1;
    
    patch([currentStart*timeframe/length(tAxis)-0.25 currentStart*timeframe/length(tAxis)+0.125  currentStart*timeframe/length(tAxis)+0.125 currentStart*timeframe/length(tAxis)-0.25],[0 0 1 1],'k',...
    'facecolor','r','edgecolor','r','FaceAlpha',0.5);
   
end

for i = 1:length(movements.countDown)
    currentStartIndex = 1;
    if i > 1
        currentStartIndex = sum(movements.countDown(1:i-1)) + 1;
    end
    currentEndIndex = sum(movements.countDown(1:i));
    
    currentStart = tAxis(movements.recMoveDown(currentStartIndex));
    currentEnd = tAxis(movements.recMoveDown(currentEndIndex));
    
    blockedVec{maxI + i} = [movements.recMoveDown(currentStartIndex) movements.recMoveDown(currentEndIndex)];
    
    patch([currentStart currentEnd  currentEnd currentStart],[0 0 1 1],'k',...
    'facecolor','b','edgecolor','b','FaceAlpha',0.25) ;
    %rectangle('Position',[currentStart abs(poti(movements.recMoveDown(currentEndIndex)) - poti(movements.recMoveDown(currentStartIndex))) currentEnd - currentStart poti(movements.recMoveDown(currentStartIndex))],'LineWidth',1, 'edgecolor', 'r');

    
    deltaTime = round(0.35* length(tAxis) / timeframe);%previously 0.25 
    currentStart = round(currentStart* length(tAxis) / timeframe);
    currentEnd = round(currentEnd * length(tAxis) / timeframe);
    %pcd(round(currentStart-deltaTime):currentEnd) = 1;
    %pcd(round(currentStart-deltaTime):round(currentStart+deltaTime)) = 1;
    
    patch([currentStart*timeframe/length(tAxis)-0.25 currentStart*timeframe/length(tAxis)+0.125  currentStart*timeframe/length(tAxis)+0.125 currentStart*timeframe/length(tAxis)-0.25],[0 0 1 1],'k',...
    'facecolor','r','edgecolor','r','FaceAlpha',0.5);
end




end 

   %% Wavelet Plot
  
dt = 1/length(tAxis);

NumVoices = 16;
a0 = 2^(1/NumVoices);
wavCenterFreq =  centfrq('morl');        %morl
%wavCenterFreq = 4*pi/(6+sqrt(2+6^2))    %morl2
%wavCenterFreq = 5/(2*pi);               %bump
maxfreq = 35;
minfreq = 5;
minscale = wavCenterFreq/(maxfreq*dt);
maxscale = wavCenterFreq/(minfreq*dt);
minscale = floor(NumVoices*log2(minscale));
maxscale = ceil(NumVoices*log2(maxscale));
scales = a0.^(minscale:maxscale).*dt;  

%Do CWT for Biceps X
cwtBX = cwtft({allAccBX,dt},'Scales',scales,'Wavelet','bump');

%Filter Unused frequencies over all times
freqidx = (cwtBX.frequencies <7 | cwtBX.frequencies > 30);
timeidx = 1:length(timeVecX);
cwtBX.cfs(freqidx,timeidx) = 0;              %set values to nil
%xrec = icwtft(reconcwt);                   %reconstructed time signal

%Do CWT for Biceps Y
cwtBY = cwtft({allAccBY,dt},'Scales',scales,'Wavelet','bump');

%Filter Unused frequencies over all times
freqidx = (cwtBY.frequencies <7 | cwtBY.frequencies > 30);
timeidx = 1:length(timeVecX);
cwtBY.cfs(freqidx,timeidx) = 0;              %set values to nil
%xrec = icwtft(reconcwt);                   %reconstructed time signal

%Do CWT for Triceps X
cwtTX = cwtft({allAccTX,dt},'Scales',scales,'Wavelet','bump');

%Filter Unused frequencies over all times
freqidx = (cwtTX.frequencies <7 | cwtTX.frequencies > 30);
timeidx = 1:length(timeVecX);
cwtTX.cfs(freqidx,timeidx) = 0;              %set values to nil
%xrec = icwtft(reconcwt);                   %reconstructed time signal

%Do CWT for Triceps Y
cwtTY = cwtft({allAccTY,dt},'Scales',scales,'Wavelet','bump');

%Filter Unused frequencies over all times
freqidx = (cwtTY.frequencies <7 | cwtTY.frequencies > 30);
timeidx = 1:length(timeVecX);
cwtTY.cfs(freqidx,timeidx) = 0;              %set values to nil
%xrec = icwtft(reconcwt);                   %reconstructed time signal


cwtAll{1} = cwtBX;
cwtAll{2} = cwtBY; 
cwtAll{3} = cwtTX;
cwtAll{4} = cwtTY;

absoluteValues{1} = abs(cwtBX.cfs);
absoluteValues{2} = abs(cwtBY.cfs); 
absoluteValues{3} = abs(cwtTX.cfs);
absoluteValues{4} = abs(cwtTY.cfs);
%absoluteValues = [abs(cwtBX.cfs) abs(cwtBY.cfs) abs(cwtTX.cfs) abs(cwtTY.cfs)];
interestingFreqDomains = struct('minVal', [8 10 12 14 16 18 20 22 24 26 28], 'maxVal', [10 12 14 16 18 20 22 24 26 28 30]);
%Every entry in the meanPowerVecs should be one column in the traingData set
meanPowerVecs{1} = [];
for i = 1:length(cwtAll)
    for j = 1:length(interestingFreqDomains.minVal)
       powerValues = absoluteValues{i}((cwtAll{i}.frequencies > interestingFreqDomains.minVal(j) & cwtAll{i}.frequencies < interestingFreqDomains.maxVal(j)), 1:end); 
       meanPowerVec =[];
       for idx = 1:length(powerValues)
           meanPowerVec = [meanPowerVec mean(powerValues(1:end,idx))];
       end
       meanPowerVecs{j} = meanPowerVec;
    end
end

idxStart = 1;

freq810 = meanPowerVecs{1}';
freq1012 = meanPowerVecs{2}';
freq1214 = meanPowerVecs{3}';
freq1416 = meanPowerVecs{4}';
freq1618 = meanPowerVecs{5}';
freq1820 = meanPowerVecs{6}';
freq2022 = meanPowerVecs{7}';
freq2224 = meanPowerVecs{8}';
freq2426 = meanPowerVecs{9}';
freq2628 = meanPowerVecs{10}';
freq2830 = meanPowerVecs{11}';

pcd = pcd';



%moving average from the last 50ms
n = ceil(0.05* length(freq810)/timeframe);
movAvg{1} = [];
for i = 1:length(meanPowerVecs)
    movAvg{i} = tsmovavg(meanPowerVecs{i}','e',n,1);
end


%set timedelta for derivation to 150ms
n = ceil(0.15* length(freq810)/timeframe);
deltaPowerVecs = cell(1,length(meanPowerVecs));
for i = 1:length(deltaPowerVecs)
    deltaPowerVecs{i} = ones(1,n) - 1;
end

for i = 1:length(movAvg)
    for j = n+1:length(movAvg{i})
        deltaPowerVecs{i} = [deltaPowerVecs{i} movAvg{i}(j)-movAvg{i}(j-n)];
        if isnan(deltaPowerVecs{i}(j))
            deltaPowerVecs{i}(j) = 0
        end
    end
    deltaPowerVecs{i} = deltaPowerVecs{i}';
end

dfreq810 = deltaPowerVecs{1};
dfreq1012 = deltaPowerVecs{2};
dfreq1214 = deltaPowerVecs{3};
dfreq1416 = deltaPowerVecs{4};
dfreq1618 = deltaPowerVecs{5};
dfreq1820 = deltaPowerVecs{6};
dfreq2022 = deltaPowerVecs{7};
dfreq2224 = deltaPowerVecs{8};
dfreq2426 = deltaPowerVecs{9};
dfreq2628 = deltaPowerVecs{10};
dfreq2830 = deltaPowerVecs{11};


%currentTable = table(dfreq810,dfreq1012,dfreq1214,dfreq1416,dfreq1618,dfreq1820,dfreq2022,dfreq2224,dfreq2426,dfreq2628,dfreq2830,freq810,freq1012,freq1214,freq1416,freq1618,freq1820,freq2022,freq2224,freq2426,freq2628,freq2830, allAccBX, allAccBY, allAccTX, allAccTY,pcd);



%Give Back compressedTable 
compPowerVecs = cell(1,length(meanPowerVecs));
n = ceil(0.00833* length(freq810)/timeframe);
start = 1;
for idx = 1:length(meanPowerVecs)
    for i = 1:length(freq810)/n
     j = i * n;
     compPowerVecs{idx}(i) = mean(meanPowerVecs{idx}(start:j));
     start = j;
    end
    start = 1;
    compPowerVecs{idx} = compPowerVecs{idx}';
end

compDeltaPowerVecs = cell(1,length(compPowerVecs));
for idx = 1:length(compPowerVecs)
    compDeltaPowerVecs{idx}(1) = 0;
    for i = 2:length(compPowerVecs{idx})
        compDeltaPowerVecs{idx}(i) = compPowerVecs{idx}(i) - compPowerVecs{idx}(i-1);
    end
    start = 1;
    compDeltaPowerVecs{idx} = compDeltaPowerVecs{idx}';
end

start = 1;
accels = {allAccBX,allAccBY, allAccTX, allAccTY};
compAccels = cell(1,length(accels));
for idx = 1:length(accels)
    for i = 1:length(accels{idx})/n
       j = i * n;
        compAccels{idx}(i) = mean(accels{idx}(start:j));
        start = j;
    end
    start = 1;
    compAccels{idx} = compAccels{idx}';
end

compPcd = [];
for i = 1:length(pcd)/n
       j = i * n;
        currentSum = sum(pcd(start:j));
        if currentSum > 0
            compPcd(i) = 1;
        else
            compPcd(i) = 0;
        end
        start = j;
end
compPcd = compPcd';


frequs = {freq810,freq1012,freq1214,freq1416,freq1618,freq1820,freq2022,freq2224,freq2426,freq2628,freq2830};
dfrequs = {dfreq810,dfreq1012,dfreq1214,dfreq1416,dfreq1618,dfreq1820,dfreq2022,dfreq2224,dfreq2426,dfreq2628,dfreq2830};

%Plot for Ausarbeitung
%figure(2)
%plot(tAxis, poti)
%axis([0 10 0 1])
%hold on;
%plot(tAxis,pcd,'r')
%legend('Potentiometer-Wert','Zielvariable')


compressed = 1;

if compressed
    freq810 = compPowerVecs{1};
    freq1012 = compPowerVecs{2};
    freq1214 = compPowerVecs{3};
    freq1416 = compPowerVecs{4};
    freq1618 = compPowerVecs{5};
    freq1820 = compPowerVecs{6};
    freq2022 = compPowerVecs{7};
    freq2224 = compPowerVecs{8};
    freq2426 = compPowerVecs{9};
    freq2628 = compPowerVecs{10};
    freq2830 = compPowerVecs{11};
    
    dfreq810 = compDeltaPowerVecs{1};
    dfreq1012 = compDeltaPowerVecs{2};
    dfreq1214 = compDeltaPowerVecs{3};
    dfreq1416 = compDeltaPowerVecs{4};
    dfreq1618 = compDeltaPowerVecs{5};
    dfreq1820 = compDeltaPowerVecs{6};
    dfreq2022 = compDeltaPowerVecs{7};
    dfreq2224 = compDeltaPowerVecs{8};
    dfreq2426 = compDeltaPowerVecs{9};
    dfreq2628 = compDeltaPowerVecs{10};
    dfreq2830 = compDeltaPowerVecs{11};
    
    allAccBX = compAccels{1};
    allAccBY = compAccels{2};
    allAccTX = compAccels{3}; 
    allAccTY = compAccels{4};
    
    pcd = compPcd;
end


currentTable = table(dfreq810,dfreq1012,dfreq1214,dfreq1416,dfreq1618,dfreq1820,dfreq2022,dfreq2224,dfreq2426,dfreq2628,dfreq2830,freq810,freq1012,freq1214,freq1416,freq1618,freq1820,freq2022,freq2224,freq2426,freq2628,freq2830, allAccBX, allAccBY, allAccTX, allAccTY,pcd);


end