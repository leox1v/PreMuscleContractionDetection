function ShowClassificationResults(originalD, classifiedD, id, weightStr)

isEMG = 0;
if length(strfind(originalD, 'NoWeight')) > 0
     weightStr = '0KG';
elseif length(strfind(originalD, '5KG')) > 0
    weightStr = '5KG';
end
if length(strfind(originalD, 'EMG')) > 0
    isEMG = 1;
end


T = readtable(strcat('../Data/PFD/',weightStr,'/forTesting/',originalD));
T_pred = readtable(strcat('../Data/PFD/',weightStr,'/forTesting/',classifiedD));

timeframe = 10;
linearPotiValue = 4.8;

x = getVoltageValue(T.BX);
y = getVoltageValue(T.BY);
z = getVoltageValue(T.TX);
tAxis = T.time / 1000;
poti = getVoltageValue(T.poti);
if isEMG
    emg = getVoltageValue(T.EMG);
    newEMG = [];
for c = 1:length(emg)
    currentEMGValue = emg(c);
    normalizedValue = (currentEMGValue - 1.55) /0.3;
      if normalizedValue > 1
          normalizedValue = 1;
      elseif normalizedValue < 0
              normalizedValue = 0;
      end
    newEMG = [newEMG, normalizedValue];
end 

emg = newEMG;
end
predTrue = T_pred.Var3; %the classified precontraction from RF
predRec = T_pred.Var2;  %the real recorded precontraction of the muscle derived from the poti value

allAccX = getGValue(x);
allAccY = getGValue(y);
allAccZ = getGValue(z);

timeVecX = linspace(0,timeframe, length(allAccX));
timeVecY = linspace(0,timeframe, length(allAccY));
timeVecZ = linspace(0,timeframe, length(allAccZ));

comBlockedVec = zeros(length(allAccX),1);

newPoti = [];
for c = 1:length(poti)
    currentPotiValue = poti(c);
    normalizedValue = -(currentPotiValue - linearPotiValue) /2.5;
      if normalizedValue > 1
          normalizedValue = 1;
      end
    newPoti = [newPoti, normalizedValue];
end

poti = newPoti;

    figure(1)
    
    plot(tAxis, poti);
    xlabel('Zeit [s]');
    ylabel('Muskelkontraktion');
    axis([0, timeframe, 0,1]);
    grid;
    drawnow;
    if isEMG
        hold on;
        plot(tAxis, emg);
    end
    
    %Find and plot movements of the arm using a smart derivation from the
%PotiValues
movements = findMovements(poti, timeframe, tAxis);
maxI = 0;
blockedVec{1} = [];
moveUpStart = [];

for i = 1:length(movements.countUp)
    currentStartIndex = 1;
    if i > 1
        currentStartIndex = sum(movements.countUp(1:i-1)) + 1;
    end
    currentEndIndex = sum(movements.countUp(1:i));
    
    currentStart = tAxis(movements.recMoveUp(currentStartIndex));
    currentEnd = tAxis(movements.recMoveUp(currentEndIndex));
    
    comBlockedVec(round((currentStart+0.2) * length(allAccX) / timeframe):round(currentEnd * length(allAccX) / timeframe)) = 1;
    
    blockedVec{i} = [movements.recMoveUp(currentStartIndex) movements.recMoveUp(currentEndIndex)];
    maxI = i;
    
    moveUpStart = [moveUpStart currentStart];
    %patch([currentStart currentEnd  currentEnd currentStart],[0 0 1 1],'k',...
    %'facecolor','g','edgecolor','g','FaceAlpha',0.25) ;
   
end

for i = 1:length(movements.countDown)
    currentStartIndex = 1;
    if i > 1
        currentStartIndex = sum(movements.countDown(1:i-1)) + 1;
    end
    currentEndIndex = sum(movements.countDown(1:i));
    
    currentStart = tAxis(movements.recMoveDown(currentStartIndex));
    currentEnd = tAxis(movements.recMoveDown(currentEndIndex));
    
    blockedVec{maxI + i} = [movements.recMoveDown(currentStartIndex) movements.recMoveDown(currentEndIndex)];
    
    comBlockedVec(round((currentStart-0.15) * length(allAccX) / timeframe):round((currentEnd + 0.15) * length(allAccX) / timeframe)) = 1;
    
    %patch([currentStart currentEnd  currentEnd currentStart],[0 0 1 1],'k',...
    %'facecolor','b','edgecolor','b','FaceAlpha',0.25) ;
    %rectangle('Position',[currentStart abs(poti(movements.recMoveDown(currentEndIndex)) - poti(movements.recMoveDown(currentStartIndex))) currentEnd - currentStart poti(movements.recMoveDown(currentStartIndex))],'LineWidth',1, 'edgecolor', 'r');
end

hold on

yyaxis right;
axis([0, timeframe, 0,1]);
ylabel('Klassifikationsentscheidung');
tAxis = [linspace(0,timeframe, length(predTrue))];
plot(tAxis,predTrue);
%plot(tAxis,predRec * 0.8 + 0.1,'y');

deltaC = abs(predTrue - predRec);
q = trapz(tAxis,deltaC);


%fuzzy 
tic
myFis = readfis('fuzzy1.fis');
myFis2 = readfis('fuzzy2.fis');
myFis3 = readfis('fuzzy3.fis');
fisRes = [];
for i = 1:length(predTrue)
    fisRes = [fisRes evalfis([predTrue(i) poti(i) comBlockedVec(i)], myFis)];
end

fisRes2 = [fisRes(1) evalfis([predTrue(2) poti(2) comBlockedVec(2) fisRes(1)], myFis2)];
for i = 3:length(fisRes)
    fisRes2 = [fisRes2 evalfis([predTrue(i) poti(i) comBlockedVec(i) fisRes(i-1) fisRes(i-2)], myFis3)];
end

res = gt(fisRes2,0.08);
timeinterval = toc;

%plot(tAxis,fisRes,'g','LineWidth',2);
%plot(tAxis,fisRes2,'b','LineWidth',2);
plot(tAxis,res,'r','LineWidth',2);
if id == 1
    legend('Auslenkung gemessen durch Potentiometer',... 
    'Kontraktion erkannt von Klassifikator',...
    'Klassifikation nach FIS', 'Bin�re Entscheidung');
end
hold off % reset hold state
   




end