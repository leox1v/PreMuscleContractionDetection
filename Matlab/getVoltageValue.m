function voltage = getVoltageValue(u)
 voltage = u * (5.0 / 1023.0);
end