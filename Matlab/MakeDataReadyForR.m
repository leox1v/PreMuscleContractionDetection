% if forcePreprocessing is TRUE, the data marked as training will be
% preprocessed too
function MakeDataReadyForR(weightStr, forcePreprocessing) 
    PreprocessData(weightStr, forcePreprocessing);
    CreateTargetData(weightStr);
end