function timeseriesCleaned = cleanTimeSeries(timeseries, blockedVec)

for i = 1:length(timeseries.minValues)
    timeSVecNat{i} = timeseries.minValues(i):timeseries.minValues(i)+timeseries.distValues(i);
end

%sort blockedVec
entries = cellfun(@(x) x(1), blockedVec);
[dummy,index] = sort(entries); 
blockedVec = blockedVec(index);

for i = 1:length(blockedVec)
    blockedVec{i} = blockedVec{i}(1):blockedVec{i}(2);
end


duplic = [];

if length(timeseries.minValues) > 0 & length(blockedVec) > 0
    for i = 1:length(blockedVec)
        for j = 1:length(timeSVecNat)
           duplic = [duplic intersect(blockedVec{i},timeSVecNat{j})];
        end
    end
end

%Entferne Werte aus timeseries, welche in duplic vorkommen und gib
%dann timeseries als cleaned zur�ck!
% for i = 1:length(timeseries.minValues)
%     currentDuplic = find(duplic >= timeseries.minValues(i) & duplic <= timeseries.minValues(i)+timeseries.distValues(i))
%     if length(currentDuplic) > 0
%       timeseries.distValues(i) = duplic(min(currentDuplic)) - timeseries.minValues(i)
%       for j = 1:length(timeSVecNat)
%         if ismember(currentDuplic(end) + 1, timeSVecNat{i})
%             timeseries.minValues = [timeseries.minValues currentDuplic(end) + 1];
%             timeseries.distValues = [timeseries.distValues currentDuplic(end) + 1];
%         end
%       end
%     end
% end

idxToDelete = [];
newMins = [];
newDists = [];
for i = 1:length(timeseries.minValues)
    currentDuplicIdc = find(duplic >= timeseries.minValues(i) & duplic <= timeseries.minValues(i)+timeseries.distValues(i))
    currentDuplic = duplic(currentDuplicIdc);
    if length(currentDuplic) > 0
        if currentDuplic(1) == timeseries.minValues(i) & currentDuplic(end) == timeseries.minValues(i) + timeseries.distValues(i)
            %1
            %just delete current
            idxToDelete = [idxToDelete i];
        elseif currentDuplic(1) == timeseries.minValues(i)
            %2
            delta = currentDuplic(end) - timeseries.minValues(i);
            timeseries.minValues(i) = currentDuplic(end);
            timeseries.distValues(i) = timeseries.distValues(i) - delta;
        elseif currentDuplic(end) == timeseries.minValues(i) + timeseries.distValues(i)
            %3
            timeseries.distValues(i) = currentDuplic(1) - timeseries.minValues(i);
        else
            %4
            %splitInSubvectors
            vecCount = 1
            splittedVec{vecCount} = [currentDuplic(1)];
            for k = 2:length(currentDuplic)
                if currentDuplic(k) - 1 == currentDuplic(k-1)
                    splittedVec{vecCount} = [splittedVec{vecCount} currentDuplic(k)];
                else
                    vecCount = vecCount + 1;
                    splittedVec{vecCount} = [currentDuplic(k)];
                end
            end
            idxToDelete = [idxToDelete i];
            
            if length(splittedVec) == 1
                newMins = [newMins timeseries.minValues(i) currentDuplic(end)];
                newDists = [newDists currentDuplic(1)-timeseries.minValues(i) timeseries.minValues(i)+timeseries.distValues(i)-currentDuplic(end)];
            elseif length(splittedVec) > 1
                %Added first point/dist manually
                 newMins = [newMins timeseries.minValues(i)];
                 newDists = [newDists currentDuplic(1)-timeseries.minValues(i)];
                for j = 2:length(splittedVec)-1
                    newMins = [newMins splittedVec{j-1}(end)];
                    newDists = [newDists splittedVec{j}(1)-splittedVec{j-1}(end)];
                end
                newMins = [newMins splittedVec{end}(end)];
                newDists = [newDists timeseries.minValues(i)+timeseries.distValues(i)-currentDuplic(end)];
            end
        end
    end
end

        

timeseries.minValues(idxToDelete) = [];
timeseries.distValues(idxToDelete) = [];
timeseries.minValues = [timeseries.minValues newMins];
timeseries.distValues = [timeseries.distValues newDists];
            


indexesToDelete = find(timeseries.distValues == 0);
timeseries.distValues(indexesToDelete) = [];
timeseries.minValues(indexesToDelete) = [];

[uniqueValues, idx] = unique(timeseries.minValues);
timeseries.minValues = timeseries.minValues(idx);
timeseries.distValues = timeseries.distValues(idx);


newMinValues = [];
newDistValues = [];
newEntryIdx = [];
indexesToDelete = [];
if length(timeseries.minValues) > 1
    for i = 1:length(timeseries.minValues)
        if i < length(timeseries.minValues)
            if timeseries.minValues(i) + timeseries.distValues(i) > timeseries.minValues(i+1)
                newMinValues = [newMinValues timeseries.minValues(i)];
                %endPoint = min([timeseries.minValues(i+1) timeseries.minValues(i) + timeseries.distValues(i)]);
                endPoint = timeseries.minValues(i) + timeseries.distValues(i);
                if endPoint - timeseries.minValues(i) <= 0 
                    endPoint = timeseries.minValues(i+1);
                end
                newDistValues = [newDistValues endPoint - timeseries.minValues(i)];
                newEntryIdx = [newEntryIdx i];
                indexesToDelete = [indexesToDelete i i+1];
            end
        end
    end
end

indexesToDelete = unique(indexesToDelete);

if length(indexesToDelete) > 0
    timeseries.minValues(indexesToDelete) = [];
    timeseries.distValues(indexesToDelete) = [];
end

if length(newMinValues) > 0 & length(newDistValues) > 0
    for i = 1: length(newEntryIdx)
        timeseries.minValues = [timeseries.minValues(1:newEntryIdx(i) - 1) newMinValues(i) timeseries.minValues(newEntryIdx(i):end)];
        timeseries.distValues = [timeseries.distValues(1:newEntryIdx(i) - 1) newDistValues(i) timeseries.distValues(newEntryIdx(i):end)];
    end
end
            
x = find(timeseries.distValues <= 0);
timeseries.minValues(x) = [];
timeseries.distValues(x) = [];


timeseriesCleaned = timeseries;
