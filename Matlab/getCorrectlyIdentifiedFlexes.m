function s = getCorrectlyIdentifiedFlexes(minXRight,maxXRight,minXTest,maxXTest)

correctlyIdentified = [];
if length(minXTest) > 0
    j= 1;
    for i = 1:length(minXRight)
        if j <= length(minXTest) && i <=length(minXRight)
            if (minXTest(j) < maxXRight(i) & minXTest(j) > minXRight(i)) | (maxXTest(j) < maxXRight(i) & maxXTest(j) > minXRight(i)) | minXTest(j) > minXRight(i) & maxXTest(j) < maxXRight(i) | minXTest(j) < minXRight(i) & maxXTest(j) > maxXRight(i) %| minXRight(i) < minXTest(j) & maxXRight(i) > minXTest(j)
               correctlyIdentified = [correctlyIdentified true];
            j = j+1;
            else
             correctlyIdentified = [correctlyIdentified false];
            end
        else
            correctlyIdentified = [correctlyIdentified false];
        end
    end
else
    for i = 1:length(minXRight)
        correctlyIdentified = [correctlyIdentified 0];
    end
end

s = struct('correctlyIdentified', correctlyIdentified);

end
