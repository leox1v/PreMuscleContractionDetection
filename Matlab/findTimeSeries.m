   function timeSeries = findTimeSeries(colsOfPotHighs, sizeTAxis, timeframe)
 
   if length(colsOfPotHighs) == 0
       timeSeries = struct('minValues', [], 'distValues', []);
   else
       
   uniqueValues = unique(colsOfPotHighs)
   minValues = [];
   maxValues = [];
   currentStartValue = min(uniqueValues);
   similarity = 1;
   deltaDist = 0.3 * sizeTAxis / timeframe;
   count = 0;
   noNewMax = 0;
   
   while currentStartValue < uniqueValues(end)
       if count ~= 0
           %minValues = [minValues currentStartValue];
           count = 0;
           noNewMax = 0;
       else
           if length(minValues) == 0
               minValues = currentStartValue
           else
               minValues(end) = currentStartValue;
           end
           noNewMax = 1;
       end
           newVec = currentStartValue:currentStartValue + deltaDist;
           similarity = sum(ismember(newVec, colsOfPotHighs)) / length(newVec);
           
           if similarity > 0.9
               count = count +1;
               currentStartValue = newVec(end);
           else
               if noNewMax == 0
                    maxValues = [maxValues currentStartValue];
                    minValues = [minValues currentStartValue];
               end
               currentStartValue = newVec(end);
           end
   end
   
   if abs(uniqueValues(end) - sizeTAxis(end)) < 10
       if length(minValues) == length(maxValues) + 1
            maxValues = [maxValues uniqueValues(end)];
       end
   else
       minValues(end) = []
   end
   
  
   if size(minValues) > 0 & size(maxValues) > 0 & size(minValues) == size(maxValues)
        distValues = maxValues - minValues;
        s = struct('minValues', minValues, 'distValues', distValues);
        timeSeries = s;
   else
       timeSeries = struct('minValues', [], 'distValues', []);
   end
   end
   
           
              
           
       
   end