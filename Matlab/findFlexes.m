function s = findFlexes(poti, tAxis, timeframe)
 [pks loc] = findpeaks(poti, tAxis, 'MinPeakDistance', 2);
 indexesToDelete = pks < 0.4;
 pks(indexesToDelete) = [];
 loc(indexesToDelete) = [];
 
 maxY = pks;
 maxX = loc;
 
deltaDist = round(2.5* length(tAxis) / timeframe);
 
 minX = [];
 minY = [];
 
 for i = 1:length(maxX)
     if find(tAxis == maxX(i)) < deltaDist/4
        currentXPart = find(tAxis == maxX(i)) - deltaDist : find(tAxis == maxX(i));
     else
        currentXPart = find(tAxis == maxX(i)) - deltaDist : find(tAxis == maxX(i)) - deltaDist/4;
     end
     currentXPart(currentXPart < 1) = [];
     [val loc] = min(poti(currentXPart));
     
     minY = [minY val];
     minX = [minX tAxis(currentXPart(loc))];
 end
 
 deltaY = maxY - minY;
 indexesToDelete = find(deltaY < 0.2);
 minX(indexesToDelete) = [];
 maxX(indexesToDelete) = [];
 minY(indexesToDelete) = [];
 maxY(indexesToDelete) = [];
 
 s = struct('minX', minX, 'minY', minY, 'maxX' , maxX, 'maxY', maxY);
end